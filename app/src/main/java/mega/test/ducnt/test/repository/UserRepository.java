package mega.test.ducnt.test.repository;

import mega.test.ducnt.test.network.callback.IPresenterCallback;
import mega.test.ducnt.test.network.connection.Connection;
import mega.test.ducnt.test.network.connection.Response;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UserRepository {
    public static final String USER_NAME = "touchpad";
    public static final String PASSWORD = "trantung1";
    private final Connection mConnection;

    public UserRepository() {
        mConnection = new Connection();
    }

    public void login(String key, IPresenterCallback iPresenterCallback) {
        String url = "http://en.megamu.net/login";
        MultipartBody.Builder buider = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        buider.addFormDataPart("login", key);
        buider.addFormDataPart("password", PASSWORD);
        RequestBody requestBody = buider.build();
        mConnection.post(url,requestBody, new Response(iPresenterCallback));
    }

    public void getData(IPresenterCallback iPresenterCallback) {
        String url = "http://en.megamu.net/panel";
        mConnection.get(url,null,new Response(iPresenterCallback));
    }
}

