package mega.test.ducnt.test.network.connection;


import java.io.IOException;

import mega.test.ducnt.test.exception.ServerException;
import mega.test.ducnt.test.network.callback.IPresenterCallback;
import okhttp3.Call;
import okhttp3.Callback;



public class Response implements Callback {

    private final IPresenterCallback mNetworkCallback;

    public Response(IPresenterCallback callback) {
        mNetworkCallback = callback;
    }

    @Override
    public void onFailure(Call call, IOException e) {
        if (mNetworkCallback == null) return;
        mNetworkCallback.onError(new ServerException());
    }

    @Override
    public void onResponse(Call call, okhttp3.Response response) throws IOException {
        if (mNetworkCallback == null) return;
        String body = response.body().string();
        mNetworkCallback.onResponse(body);
    }

}
