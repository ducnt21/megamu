package mega.test.ducnt.test;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreference {


    private final static String PREF_NAME = "Mega_mu";


    private final Context mContext;

    public SharedPreference(Context context) {
        mContext = context;
    }

    public boolean saveString(String key, String userInfo) {
        if (mContext.getSharedPreferences(PREF_NAME, 0) != null) {
            SharedPreferences.Editor e = mContext.getSharedPreferences(PREF_NAME, 0)
                    .edit();
            e.putString(key, userInfo);
            e.apply();
            return true;
        }
        return false;
    }

    public String getString(String key) {
        return mContext.getSharedPreferences(PREF_NAME, 0).getString(key, "");
    }


    public void clear(String key){
        SharedPreferences preferences = mContext.getSharedPreferences(PREF_NAME, 0);
        preferences.edit().remove(key).apply();
    }
}
