package mega.test.ducnt.test.network.callback;


public interface IPresenterCallback {

    void onError(Exception exception);

    void onResponse(String response);

}
