package mega.test.ducnt.test.exception;


public class ServerException extends Exception implements IException {

    private String mErrorMsg = MSG_SERVER_ERROR;

    public ServerException() {

    }

    public ServerException(String msg) {
        mErrorMsg = msg;
    }

    @Override
    public String getMessage() {
        return mErrorMsg;
    }
}
