package mega.test.ducnt.test;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.regex.Pattern;

import mega.test.ducnt.test.network.callback.IPresenterCallback;
import mega.test.ducnt.test.repository.UserRepository;

import static mega.test.ducnt.test.repository.UserRepository.USER_NAME;

public class Presenter {

    public static final String XTREME = "http://www.xtremetop100.com";


    private final MainActivity mView;
    private final SharedPreference mSharedPreference;
    private final UserRepository mUserRepo;
    private int mCurrent = 0;

    public Presenter(MainActivity mainActivity) {
        mView = mainActivity;
        mSharedPreference = new SharedPreference(mView);
        mUserRepo = new UserRepository();
//        login(1);
    }

    public void login(final int i) {
        String userSetAccount = mView.getCurrentAccountEd();
        final String key;
        if (TextUtils.isEmpty(userSetAccount)) {
            key = USER_NAME + i;
            mCurrent = i;
        } else {
            key = USER_NAME + userSetAccount;
            mCurrent = Integer.parseInt(userSetAccount);
        }
        mView.setCurrentAccount(key);
        mView.clearCurrentAccountEd();
        mUserRepo.login(key, new IPresenterCallback() {
            @Override
            public void onError(Exception exception) {
                mView.showError(key);
            }

            @Override
            public void onResponse(String response) {
                int first = response.indexOf(XTREME);
                int lastPos = response.indexOf("\"", first);
                String url = response.substring(first, lastPos);
                mView.loadWebView(url);
            }
        });
    }


    public void next() {
        mCurrent++;
        login(mCurrent);
    }

    public void reload() {
        mCurrent = 0;
    }
}
