package mega.test.ducnt.test.network.connection;


import android.util.Log;
import android.webkit.CookieManager;

import org.json.JSONObject;

import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class Connection {

    private static final String CONTENT_TYPE_LABEL = "Accept";
    private static final String CONTENT_TYPE_VALUE_JSON = "application/json; charset=utf-8";
    private OkHttpClient mOkHttpClient;
    private Request mRequest;
    public static final MediaType JSON
            = MediaType.parse("application/json");
    public static final MediaType URL_ENCODE = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");

    public Connection() {
        createClient();
    }

    private void createClient() {
        mOkHttpClient = new OkHttpClient();
    }

    public void get(final String url, final HashMap<String, String> params, Response response) {
        String fullUrl = getFullUrl(url, params);
        Log.d("Connection get: ", fullUrl);
        final Request.Builder builder = new Request.Builder()
                .url(fullUrl)
                .get()
                .addHeader("Accept", "application/json");
        mRequest = builder.build();
        mOkHttpClient.newCall(mRequest).enqueue(response);
    }


    public void put(final String url, HashMap<String,String> params, Response callback) {
        RequestBody requestBody = RequestBody.create(JSON, new JSONObject(params).toString());
        final Request.Builder builder = new Request.Builder()
                .url(url)
                .put(requestBody)
                .addHeader("Accept", "application/json");
        mRequest = builder.build();
        mOkHttpClient.newCall(mRequest).enqueue(callback);
    }



    public void post(final String url, HashMap<String,String> params, Response callback) {
        RequestBody requestBody = RequestBody.create(JSON, new JSONObject(params).toString());
        final Request.Builder builder = new Request.Builder()
                .url(url)
                .post(requestBody);
        mRequest = builder.build();
        mOkHttpClient.newCall(mRequest).enqueue(callback);
    }



    public void post(final String url, RequestBody params, Response callback) {

        final Request.Builder builder = new Request.Builder()
                .url(url)
                .post(params);

        mRequest = builder.build();

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cookieJar(new MyCookieJar())
                .build();
        okHttpClient.newCall(mRequest).enqueue(callback);
    }

    public class MyCookieJar implements CookieJar {

        private List<Cookie> cookies;

        @Override
        public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
            this.cookies =  cookies;
        }

        @Override
        public List<Cookie> loadForRequest(HttpUrl url) {
            if (cookies != null)
                return cookies;
            return new ArrayList<Cookie>();

        }
    }

    /**
     * Add params to url in Get Method
     */
    private String getFullUrl(String url, HashMap<String, String> params) {
        if (params == null) return url;
        StringBuilder urlParams = new StringBuilder(url + "?");
        for (Map.Entry<String, String> entry : params.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            urlParams.append(key).append("=").append(value).append("&");
        }
        String fullURL = urlParams.toString();
        if (fullURL.length() > 0) {
            fullURL = fullURL.substring(0, fullURL.length() - 1);
        }
        return fullURL;
    }

    private RequestBody hashmapToRequestBody(HashMap<String, String> params) {
        MultipartBody.Builder buider = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        if (params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                buider.addFormDataPart(key, value);
            }
        }
        return buider.build();
    }

}
