package mega.test.ducnt.test;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.lang.reflect.Method;

import io.fabric.sdk.android.Fabric;
import mega.ducnt.hack.R;


public class MainActivity extends AppCompatActivity {
    public static final String LOGIN = "http://en.megamu.net/";
    public static final String VOTE = "https://login.microsoftonline.com/";
    public static final String XTREAM = "https://login.microsoftonline.com/";
    public static final String USERNAME = "touchpad";
    public static final String PASSWORD = "trantung1";
    private WebView mWebview;
    private NetworkStateReceiver networkStateReceiver;
    private Presenter mPresenter;
    private TextView mCurrentAccountTv;
    private EditText mCurrentAccountEd;
    private String mCurrentUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        mCurrentAccountTv = (TextView) findViewById(R.id.tv_current_account);
        mCurrentAccountEd = (EditText) findViewById(R.id.ed_account);
        setReciever();
        setUpWebView();
        mPresenter = new Presenter(this);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setUpWebView() {
        mWebview = (WebView) findViewById(R.id.activity_main);
        mWebview.setWebViewClient(new Client());
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.getSettings().setDomStorageEnabled(true);
//        mWebview.getSettings().setUseWideViewPort(true);
        mWebview.getSettings().setLoadWithOverviewMode(true);
        mWebview.getSettings().setBuiltInZoomControls(true);
        mWebview.getSettings().setDisplayZoomControls(false);
        mWebview.setInitialScale(220);
        mWebview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
    }

    public void showError(final String finalI) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, "Con so " + finalI + "teo cmnr", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String getCurrentAccountEd(){
        return mCurrentAccountEd.getText().toString();
    }

    public void loadWebView(final String url) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCurrentUrl = url;
                mWebview.loadUrl(url);
            }
        });
    }

    void playRingTone(){
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        r.play();
    }


    public void setMobileDataState(boolean mobileDataEnabled) {
        try {
            TelephonyManager telephonyService = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

            Method setMobileDataEnabledMethod = telephonyService.getClass().getDeclaredMethod("setDataEnabled", boolean.class);

            if (null != setMobileDataEnabledMethod) {
                setMobileDataEnabledMethod.invoke(telephonyService, mobileDataEnabled);
            }
        } catch (Exception ex) {
            Log.e("DucNT", "Error setting mobile data state", ex);
        }
    }

    public void reload(View view) {
        mPresenter.reload();
        setMobileDataState(false);
    }

    public void next(View view) {
        setMobileDataState(false);
        closeKeyboard();
    }
    void closeKeyboard(){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }


    public void setCurrentAccount(String key) {
        mCurrentAccountTv.setText(key);
    }

    public void clearCurrentAccountEd() {
        mCurrentAccountEd.setText("");
    }


    public class Client extends WebViewClient {
        @Override
        public void onLoadResource(WebView view, String url) {
            Log.d("DucNT",url);
            super.onLoadResource(view, url);
            if (url.contains("http://muonline.co/mocbanner.gif")) {
                try {
                    Thread.sleep(2000);
                    turnOff3G();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if(url.equals(mCurrentUrl)) playRingTone();
        }
    }

    private void turnOff3G() {
        Toast.makeText(this, "Tat 3g", Toast.LENGTH_SHORT).show();
        setMobileDataState(false);
    }

    private void setReciever() {
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(onOffConnectionListener());
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private NetworkStateReceiverListener onOffConnectionListener() {
        return new NetworkStateReceiverListener() {
            @Override
            public void networkAvailable() {
                mPresenter.next();
            }

            @Override
            public void networkUnavailable() {
                setMobileDataState(true);
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkStateReceiver);
    }

    public interface NetworkStateReceiverListener {
        void networkAvailable();

        void networkUnavailable();
    }

    class MyJavaScriptInterface {
        @JavascriptInterface
        @SuppressWarnings("unused")
        public void processHTML(String html) {
            // process the html as needed by the app
        }
    }
}
